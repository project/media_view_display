<?php

namespace Drupal\media_view_display\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implement config form for Media View Display.
 */
class MediaViewDisplayForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new MediaViewDisplayForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration Interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config, EntityTypeManagerInterface $entity_type_manager) {
    $this->configManager = $config;
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($config);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_view_display.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'media_view_display_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_view_display.settings');

    $bundles = [];
    try {
      $types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
      foreach ($types as $bundle) {
        $bundles[$bundle->id()] = $bundle->label();
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // @todo Put logger.
    }

    $form['container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Media Bundles'),
    ];

    $form['container']['media_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bundles'),
      '#description' => $this->t('If none selected all will be used'),
      '#options' => $bundles,
      '#default_value' => !empty($config->get('media_bundles')) ? $config->get('media_bundles') : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('media_view_display.settings')
      ->set('media_bundles', $form_state->getValue('media_bundles'))
      ->save();
  }

}
