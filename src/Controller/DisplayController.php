<?php

namespace Drupal\media_view_display\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a controller to render a file with Media View being used.
 */
class DisplayController extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration Interface.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->configManager = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view($mediaid) {
    $media = Media::load($mediaid);
    $media_bundle = $media->bundle();
    $config = $this->configManager->get('media_view_display.settings');
    $allowed_bundles = $config->get('media_bundles');
    if (!empty($config->get('media_bundles'))) {
      $allow_all_bundles = TRUE;
      foreach ($config->get('media_bundles') as $allowed_bundle) {
        if ($allowed_bundle !== 0) {
          $allow_all_bundles = FALSE;
          break;
        }
      }
      if (!$allow_all_bundles && isset($allowed_bundles[$media_bundle]) && $allowed_bundles[$media_bundle] === 0) {
        $response = new RedirectResponse($mediaid . '/edit');
        $response->send();
      }
    }
    $uri = $media->field_media_file->entity->getFileUri();
    $media_url = file_create_url($uri);
    $response = new RedirectResponse($media_url);
    $response->send();
  }

}
