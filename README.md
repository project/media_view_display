# Media View Display

This module allows for direct viewing of a file with the URL alias.
Instead of viewing the media entity and all the fields users can
view a file (like a PDF). Instead of `sites/default/files/name-of-file`
it will display the URL alias.

This is useful if a media object is referenced in multiple locations
the media object can be updated with a new file and reflected in all
instances automatically.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_view_display).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_view_display).


## Requirements

This module requires:

- PHP > 7.2
- Drupal Core Media


## Recommended modules

- [Pathauto](https://www.drupal.org/project/pathauto)
- Drupal Core Workflow


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

- Suresh Senthatti - [suresh.senthatti](https://www.drupal.org/u/suresh.senthatti)
